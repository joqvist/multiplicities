aspect MultiplicitiesMethodAccess {
	refine Java8CreateBCode public void MethodAccess.createBCode(CodeGeneration gen) {
		if (!hasPrevExpr()) {
			refined(gen);
			return;
		}
		Multiplicity sourceMult = qualifyingType().multiplicity();
		TypeDecl qualifierType = qualifyingType().relatedType();

		// TODO fix duplicated code! This is very similar to VarAcess...
		MethodDecl methodDecl = decl();
		MethodDecl decl = methodDecl.erasedMethod();
		TypeDecl returnType = methodDecl.type();

		TypeDecl typeCollection = lookupType("java.util", "Collection");
		if (sourceMult == Multiplicity.ANY) {
			TypeDecl typeIterable = lookupType("java.lang", "Iterable");
			TypeDecl typeIterator = lookupType("java.util", "Iterator");
			MethodDecl iteratorMethod = findMethod(typeIterable, "iterator");
			MethodDecl hasNextMethod = findMethod(typeIterator, "hasNext");
			MethodDecl nextMethod = findMethod(typeIterator, "next");

			int cond_label = hostType().constantPool().newLabel();
			int end_label = hostType().constantPool().newLabel();

			if (returnType.isMultiplicity()) {
				// must build result collection

				createLoadQualifier(gen);
				// [stack]: obj
				iteratorMethod.emitInvokeMethod(gen, typeIterable);
				// [stack]: iter

				// push new collection
				TypeDecl listType = getDefaultListType();
				ConstructorDecl constructor = getConstructor(listType, 0);
				listType.emitNew(gen);
				gen.emitDup();
				constructor.emitInvokeConstructor(gen);
				// [stack]: iter coll

				gen.addLabel(cond_label);

				// [stack]: iter coll
				gen.emit(Bytecode.DUP_X1);
				// [stack]: coll iter coll
				gen.emit(Bytecode.SWAP);
				// [stack]: coll coll iter
				gen.emitDup();
				// [stack]: coll coll iter iter
				hasNextMethod.emitInvokeMethod(gen, typeIterator);
				// [stack]: coll coll iter I
				gen.emitCompare(Bytecode.IFEQ, end_label);
				// [stack]: coll coll iter
				gen.emitDup();
				// [stack]: coll coll iter iter
				nextMethod.emitInvokeMethod(gen, typeIterator);
				gen.emitCheckCast(qualifierType);
				// [stack]: coll coll iter obj
				gen.emit(Bytecode.SWAP);
				// [stack]: coll coll obj iter
				gen.emit(Bytecode.DUP_X2);
				// [stack]: coll iter coll obj iter
				gen.emitPop();
				// [stack]: coll iter coll obj

				for (int i = 0; i < getNumArg(); ++i) {
					getArg(i).createBCode(gen);
					getArg(i).type().emitCastTo(gen, methodDecl.getParameter(i).type());
				}
				// [stack]: coll iter coll obj [args]

				if (!decl.isStatic() && isQualified() && prevExpr().isSuperAccess()) {
					if (!hostType().instanceOf(qualifierType)) {
						decl.createSuperAccessor(superAccessorTarget()).emitInvokeMethod(gen, superAccessorTarget());
					} else {
						decl.emitInvokeSpecialMethod(gen, qualifierType);
					}
				} else {
					decl.emitInvokeMethod(gen, qualifierType);
				}
				// [stack]: coll iter coll obj
				if (returnType instanceof AnyType) {
					MethodDecl addAllMethod = getMethod(typeCollection, "addAll", 1);
					addAllMethod.emitInvokeMethod(gen, typeCollection);
					// [stack]: coll iter I
					gen.emitPop();
				} else {
					MethodDecl addMethod = getMethod(typeCollection, "add", 1);
					// TODO fix duplicated code!
					int inner_null_label = hostType().constantPool().newLabel();
					int inner_end_label = hostType().constantPool().newLabel();
					gen.emitDup();
					gen.emitCompare(Bytecode.IFNULL, inner_null_label);
					addMethod.emitInvokeMethod(gen, typeCollection);
					gen.emitPop();// pop add() return value
					gen.emitGoto(inner_end_label);
					gen.addLabel(inner_null_label);
					gen.changeStackDepth(2);// two more stack than before GOTO
					gen.emitPop();// pop source object ref
					gen.emitPop();// pop destination collection ref
					gen.addLabel(inner_end_label);
				}
				// [stack]: coll iter
				gen.emit(Bytecode.SWAP);
				// [stack]: iter coll
				gen.emitGoto(cond_label);
				gen.addLabel(end_label);
				gen.changeStackDepth(1);// one more stack than before GOTO
				// [stack]: coll coll iter
				gen.emitPop();
				gen.emitPop();
				// [stack]: coll
			} else {
				// don't collect result values
				createLoadQualifier(gen);
				iteratorMethod.emitInvokeMethod(gen, typeIterable);
				gen.addLabel(cond_label);
				gen.emitDup();
				hasNextMethod.emitInvokeMethod(gen, typeIterator);
				gen.emitCompare(Bytecode.IFEQ, end_label);
				gen.emitDup();
				nextMethod.emitInvokeMethod(gen, typeIterator);

				gen.emitCheckCast(qualifierType);

				for (int i = 0; i < getNumArg(); ++i) {
					getArg(i).createBCode(gen);
					getArg(i).type().emitCastTo(gen, methodDecl.getParameter(i).type());
				}

				if (!decl.isStatic() && isQualified() && prevExpr().isSuperAccess()) {
					if (!hostType().instanceOf(qualifierType)) {
						decl.createSuperAccessor(superAccessorTarget()).emitInvokeMethod(gen, superAccessorTarget());
					} else {
						decl.emitInvokeSpecialMethod(gen, qualifierType);
					}
				} else {
					decl.emitInvokeMethod(gen, qualifierType);
				}

				// pop return value
				returnType.emitPop(gen);

				gen.emitGoto(cond_label);
				gen.addLabel(end_label);
				gen.changeStackDepth(1);// one more stack than before GOTO
				gen.emitPop();
			}
		} else if (sourceMult == Multiplicity.OPTION) {
			int null_label = hostType().constantPool().newLabel();
			int end_label = hostType().constantPool().newLabel();

			createLoadQualifier(gen);

			//[stack]: qual

			gen.emitDup();
			//[stack]: qual qual
			gen.emitCompare(Bytecode.IFNULL, null_label);
			//[stack]: qual
			gen.emitCheckCast(qualifierType);
			//[stack]: qual
			for (int i = 0; i < getNumArg(); ++i) {
				getArg(i).createBCode(gen);
				getArg(i).type().emitCastTo(gen, methodDecl.getParameter(i).type());
			}
			//[stack]: qual [args]

			if (!decl.isStatic() && isQualified() && prevExpr().isSuperAccess()) {
				if (!hostType().instanceOf(qualifierType)) {
					decl.createSuperAccessor(superAccessorTarget()).emitInvokeMethod(gen, superAccessorTarget());
				} else {
					decl.emitInvokeSpecialMethod(gen, qualifierType);
				}
			} else {
				decl.emitInvokeMethod(gen, qualifierType);
			}

			boolean collectResult = returnType.isMultiplicity();

			if (!collectResult) {
				// pop return value
				returnType.emitPop(gen);
			}

			gen.emitGoto(end_label);

			gen.addLabel(null_label);
			gen.changeStackDepth(1);
			//[stack]: null
			if (!collectResult) {
				gen.emit(Bytecode.POP);
			}

			gen.addLabel(end_label);

			if (collectResult) {
				gen.emitCheckCast(returnType);
			}
		} else {
			refined(gen);
		}
	}
}

