<YYINITIAL> {
  "@any"    { return sym(Terminals.ANY); }
  "@option" { return sym(Terminals.OPTION); }
  "@bare"   { return sym(Terminals.BARE); }
  "@one"    { return sym(Terminals.ONE); }
}
