aspect MultiplicityTypeAnalysis {

	refine TypeAnalysis eq VariableDeclaration.type() {
		return getModifiers().multiplicityType(refined());
	}

	refine TypeAnalysis eq FieldDeclaration.type() {
		return getModifiers().multiplicityType(refined());
	}

	refine TypeAnalysis eq ParameterDeclaration.type() {
		return getModifiers().multiplicityType(refined());
	}

	refine TypeAnalysis eq MethodDecl.type() {
		return getModifiers().multiplicityType(refined());
	}

	// TODO add error check for multiple multiplicity modifiers!!
	
	syn boolean Modifiers.hasMultiplicity() {
		for (int i = 0; i < getNumModifier(); ++i) {
			Modifier mod = getModifier(i);
			if (mod.isMultiplicity()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @return {@code true} if this type is a multiplicity type,
	 * {@code false} if it is bare or none
	 */
	syn boolean Modifier.isMultiplicity() = false;
	eq MultiplicityAnnotation.isMultiplicity() = true;
	eq BareAnnotation.isMultiplicity() = false;

	syn Multiplicity TypeDecl.multiplicity() =
		Multiplicity.BARE;

	syn Multiplicity VoidType.multiplicity() =
		Multiplicity.NONE;

	syn Multiplicity NullType.multiplicity() =
		Multiplicity.NONE;

	syn Multiplicity AnyType.multiplicity() =
		Multiplicity.ANY;

	syn Multiplicity OptionType.multiplicity() =
		Multiplicity.OPTION;

	syn Multiplicity OneType.multiplicity() =
		Multiplicity.ONE;

	/**
	 * Returns the multiplicity of the related object type,
	 * if the modifiers contain a multiplicity modifier.
	 * @param relType the type of the objects related to by the
	 * multiplicity
	 * @return the multiplicity type derived from the relType
	 */
	syn TypeDecl Modifiers.multiplicityType(TypeDecl relType) {
		for (int i = 0; i < getNumModifier(); ++i) {
			Modifier mod = getModifier(i);
			if (mod instanceof MultiplicityAnnotation) {
				MultiplicityAnnotation mult = (MultiplicityAnnotation) mod;
				return mult.lookupMultiplicityType(relType);
			}
		}
		return relType;
	}

	inh TypeDecl Modifier.lookupType(String packageName, String typeName);
	inh TypeDecl Modifier.unknownType();

	syn TypeDecl MultiplicityAnnotation.lookupMultiplicityType(TypeDecl relType);

	eq BareAnnotation.lookupMultiplicityType(TypeDecl relType) = relType;

	eq AnyAnnotation.lookupMultiplicityType(TypeDecl relType) {
		TypeDecl containerType = getContainerType().type().original();
		if (containerType.isUnknown()) {
			return unknownType();
		} else {
			return relType.anyType(containerType);
		}
	}

	eq AnyDefaultAnnotation.lookupMultiplicityType(TypeDecl relType) {
		TypeDecl containerType = lookupType("java.util", "Collection");
		if (containerType.isUnknown()) {
			return unknownType();
		} else {
			return relType.anyType(containerType);
		}
	}

	eq OptionAnnotation.lookupMultiplicityType(TypeDecl relType) =
		relType.optionType();

	eq OneAnnotation.lookupMultiplicityType(TypeDecl relType) =
		relType.oneType();

	syn nta TypeDecl TypeDecl.getAnyType(TypeDecl containerType) =
		new AnyType(new Modifiers(), "*any", new List<BodyDecl>(),
				containerType.createQualifiedAccess());

	/**
	 * Get an any(C) multiplicity type which describes a relation
	 * to objects of this type.
	 * The any(C) multiplicity requires a collection container type C.
	 */
	syn TypeDecl TypeDecl.anyType(TypeDecl containerType) =
		getAnyType(containerType);

	eq AnyType.anyType(TypeDecl containerType) =
		relatedType().anyType(containerType);

	eq OptionType.anyType(TypeDecl containerType) =
		relatedType().anyType(containerType);

	syn nta TypeDecl TypeDecl.getOptionType() =
		new OptionType(new Modifiers(), "*option", new List<BodyDecl>());

	syn nta TypeDecl TypeDecl.getOneType() =
		new OneType(new Modifiers(), "*one", new List<BodyDecl>());

	/**
	 * Get an option multiplicity type which describes a relation
	 * to objects of this type.
	 */
	syn TypeDecl TypeDecl.optionType() = getOptionType();

	eq AnyType.optionType() = relatedType().getOptionType();

	eq OptionType.optionType() = this;

	syn TypeDecl TypeDecl.oneType() = getOneType();

	eq AnyType.oneType() = relatedType().getOneType();

	eq OptionType.oneType() = this;

	/**
	 * Cannot use enclosingType because it returns null for non-bodydecls
	 */
	inh TypeDecl MultiplicityType.enclosingTypeOfMult();
	eq TypeDecl.getChild().enclosingTypeOfMult() = this;
	eq CompilationUnit.getChild().enclosingTypeOfMult() = unknownType();
	eq Program.getChild().enclosingTypeOfMult() = unknownType();

	inh lazy TypeDecl CompilationUnit.unknownType();

	/**
	 * @return the type which a multiplicity type relates to
	 */
	syn TypeDecl TypeDecl.relatedType() = this;
	eq MultiplicityType.relatedType() = enclosingTypeOfMult();

	/**
	 * @return {@code true} if this type has option, any or one multiplicity.
	 */
	syn boolean TypeDecl.isMultiplicity() = false;
	eq MultiplicityType.isMultiplicity() = true;

	/**
	 * Computes the type of a multiplicity wrap expression.
	 * For a collection type this gives the any-multiplicity
	 * with the collection type as the container type.
	 * @return the wrapped object type
	 */
	syn TypeDecl TypeDecl.wrappedType() {
		TypeDecl typeCollection = lookupType("java.util", "Collection");
		if (subtype(typeCollection)) {
			return collectionToMultiplicity();
		} else {
			// not a collection - create an option multiplicity
			return this.optionType();
		}
	}

	/**
	 * The wrapped type of a null expression is unknown.
	 */
	eq NullType.wrappedType() =
		unknownType();

	/**
	 * The wrapped array type is any(ArrayList) of it's element type.
	 */
	eq ArrayDecl.wrappedType() =
		componentType().anyType(lookupType("java.util", "ArrayList"));

	/**
	 * Create multiplicity type access!
	 */
	public Access AnyType.createQualifiedAccess() {
		if (isTopLevelType()) {
			return new AnyTypeAccess(
					new TypeAccess(packageName(), relatedType().name()),
					(Access) getContainerType().fullCopy());
		} else {
			return enclosingType().createQualifiedAccess().qualifiesAccess(
					new AnyTypeAccess(
						new TypeAccess(packageName(), relatedType().name()),
						(Access) getContainerType().fullCopy()));
		}
	}

	/**
	 * Create multiplicity type access!
	 */
	public Access OptionType.createQualifiedAccess() {
		if (isTopLevelType()) {
			return new OptionTypeAccess(
					new TypeAccess(packageName(), relatedType().name()));
		} else {
			return enclosingType().createQualifiedAccess().qualifiesAccess(
					new OptionTypeAccess(
						new TypeAccess(packageName(), relatedType().name())));
		}
	}

	eq AnyTypeAccess.type() = getTypeAccess().type()
		.anyType(getContainerType().type());
	eq OptionTypeAccess.type() = getTypeAccess().type().optionType();
	
	syn TypeDecl MultiplicityType.wrappedType();

	eq AnyType.wrappedType() = implementingType();
	eq OptionType.wrappedType() {
		TypeDecl typeCollection = lookupType("java.util", "Collection");
		return typeCollection.multiplicityContainerType(this);
	}
	eq OneType.wrappedType() = implementingType();

	/**
	 * Computes the underlying type of the multiplicity type.
	 * @return the underlying object type
	 */
	syn TypeDecl MultiplicityType.implementingType();

	/**
	 * The wrapped type of an any type is the container type for
	 * the actual multiplicity reference.
	 * The container type may be parameterized with the related object
	 * type.
	 */
	eq AnyType.implementingType() {
		TypeDecl containerType = getContainerType().type();
		return containerType.multiplicityContainerType(this);
	}

	eq OptionType.implementingType() {
		return relatedType();
	}

	eq OneType.implementingType() {
		return relatedType();
	}

	/**
	 * For a raw type this gives the corresponding generic type with one
	 * type argument, where the type argument is the related object type
	 * of the multiplicity.
	 * @return type of the container collection type for the multiplicity
	 */
	syn TypeDecl TypeDecl.multiplicityContainerType(MultiplicityType type) = this;

	eq RawInterfaceDecl.multiplicityContainerType(MultiplicityType type) {
		ArrayList args = new ArrayList();
		args.add(type.relatedType());
		return ((GenericTypeDecl) genericDecl()).lookupParTypeDecl(args);
	}

	eq RawClassDecl.multiplicityContainerType(MultiplicityType type) {
		ArrayList args = new ArrayList();
		args.add(type.relatedType());
		return ((GenericTypeDecl) genericDecl()).lookupParTypeDecl(args);
	}

	/**
	 * Try to find the collection element type of parameterized
	 * collection types. If the type is not parameterized then
	 * the Object type is returned.
	 * @return the type of the first type argument, or java.lang.Object if
	 * there is no type argument
	 */
	syn TypeDecl TypeDecl.collectionToMultiplicity() {
		TypeDecl collType = this;
		while (collType instanceof ClassDecl && collType.isAnonymous()) {
			ClassDecl classDecl = (ClassDecl) collType;
			if (classDecl.hasSuperClass()) {
				collType = classDecl.getSuperClass().type();
			} else {
				throw new Error("cannot compute collection multiplicity type");
			}
		}
		TypeDecl relatedType = collType.collectionElementType();
		return relatedType.anyType(collType);
	}

	protected TypeDecl TypeDecl.collectionElementType() {
		String typeName = "java.util.Collection";
		if (original().typeName().equals(typeName)) {
			// this type is java.util.Collection
			return getFirstTypeArg();
		}
		TypeDecl relatedType = getInterfaceTypeArg(typeName);
		if (relatedType != null) {
			return relatedType;
		} else {
			return typeObject();
		}
	}

	/**
	 * Find the type parameter used in a super interface declaration
	 * of the given type.
	 * @return type arg if found, null otherwise
	 */
	protected TypeDecl TypeDecl.getInterfaceTypeArg(String typeName) {
		return null;
	}

	protected TypeDecl InterfaceDecl.getInterfaceTypeArg(String typeName) {
		for (int i = 0; i < getNumSuperInterface(); ++i) {
			Access superInterface = getSuperInterface(i);
			TypeDecl type = superInterface.type();
			if (type.original().typeName().equals(typeName)) {
				return type.getFirstTypeArg();
			}
		}
		return null;
	}

	protected TypeDecl ClassDecl.getInterfaceTypeArg(String typeName) {
		for (int i = 0; i < getNumImplements(); ++i) {
			Access superInterface = getImplements(i);
			TypeDecl type = superInterface.type();
			if (type.original().typeName().equals(typeName)) {
				return type.getFirstTypeArg();
			}
		}
		if (hasSuperClass()) {
			return getSuperClass().type().getInterfaceTypeArg(typeName);
		} else {
			return null;
		}
	}

	protected TypeDecl TypeDecl.getFirstTypeArg() {
		return null;
	}

	/**
	 * Assumes the first type argument is the element type.
	 */
	protected TypeDecl ParClassDecl.getFirstTypeArg() {
		if (getNumArgument() > 0) {
			return getArgument(0).type();
		} else {
			return null;
		}
	}

	/**
	 * Assumes the first type argument is the element type.
	 */
	protected TypeDecl ParInterfaceDecl.getFirstTypeArg() {
		if (getNumArgument() > 0) {
			return getArgument(0).type();
		} else {
			return null;
		}
	}

	eq MultiplicityWrap.type() {
		TypeDecl type = getExpr().type();
		TypeDecl wrapped = type.wrappedType();
		return wrapped;
	}

	/**
	 * Cardinality expressions have integer type.
	 */
	eq MultiplicityCardinality.type() = typeInt();

	// TODO error check the multiplicity cast!
	eq MultiplicityCast.type() {
		TypeDecl relatedType;
		if (hasTypeAccess()) {
			relatedType = getTypeAccess().type();
		} else {
			relatedType = getExpr().type().relatedType();
		}
		return getAnnotation().lookupMultiplicityType(relatedType);
	}

	refine TypeCheck public void EqualityExpr.typeCheck() {
		TypeDecl left = getLeftOperand().type();
		TypeDecl right = getRightOperand().type();
		if (left.isMultiplicity() && right.isNull() ||
				left.isNull() && right.isMultiplicity()) {
			// can always compare multiplicities to null
			return;
		} else {
			refined();
		}
	}

	inh TypeDecl Stmt.unknownType();

	/**
	 * The type to the left of this expression
	 */
	inh lazy TypeDecl AbstractDot.qualifyingType();
	eq AbstractDot.getRight().qualifyingType() =
		qualifyingType().multiplicityDotType(getLeft().type());
	eq AbstractDot.getLeft().qualifyingType() = qualifyingType();
	eq Expr.getChild().qualifyingType() = unknownType();
	eq Stmt.getChild().qualifyingType() = unknownType();
	eq Program.getChild().qualifyingType() = unknownType();

	/**
	 * @return the resulting multiplicity type of accessing object of type
	 * right in object of this type
	 */
	syn TypeDecl TypeDecl.multiplicityDotType(TypeDecl right) =
		right.typeDotSomething(this);

	eq UnknownType.multiplicityDotType(TypeDecl right) = right;

	eq VoidType.multiplicityDotType(TypeDecl right) = this;

	eq AnyType.multiplicityDotType(TypeDecl right) = right.anyDotSomething(this);

	eq OptionType.multiplicityDotType(TypeDecl right) = right.optionDotSomething(this);

	syn TypeDecl TypeDecl.typeDotSomething(TypeDecl left) = this;

	syn TypeDecl TypeDecl.anyDotSomething(AnyType left) = typeVoid();
	eq VoidType.anyDotSomething(AnyType left) = this;
	eq AnyType.anyDotSomething(AnyType left) = anyType(lookupType("java.util", "ArrayList"));
	eq OptionType.anyDotSomething(AnyType left) = anyType(lookupType("java.util", "ArrayList"));

	syn TypeDecl TypeDecl.optionDotSomething(OptionType left) = typeVoid();
	eq VoidType.optionDotSomething(OptionType left) = this;
	eq AnyType.optionDotSomething(OptionType left) = this;
	eq OptionType.optionDotSomething(OptionType left) = this;

	refine TypeAnalysis eq AbstractDot.type() {
		TypeDecl left = getLeft().type();
		TypeDecl right = getRight().type();

		TypeDecl result = qualifyingType()
			.multiplicityDotType(left)
			.multiplicityDotType(right);
		return result;
	}

	/* We must refine this so that we do not send multiplicity types to the
	 * type constraint solver.
	 */
	refine GenericMethodsInference public Collection MethodAccess.computeConstraints(GenericMethodDecl decl) {
		Constraints c = new Constraints();
		// store type parameters
		for (int i = 0; i < decl.original().getNumTypeParameter(); i++) {
			c.addTypeVariable(decl.original().getTypeParameter(i));
		}

		// add initial constraints
		for (int i = 0; i < getNumArg(); i++) {
			TypeDecl A = getArg(i).type().relatedType();// handles multiplicities!
			int index = i >= decl.getNumParameter() ? decl.getNumParameter() - 1 : i;
			TypeDecl F = decl.getParameter(index).type().relatedType();// handles multiplicities!
			if (decl.getParameter(index) instanceof VariableArityParameterDeclaration
					&& (getNumArg() != decl.getNumParameter() || !A.isArrayDecl())) {
				F = F.componentType();
			}
			c.convertibleTo(A, F);
		}
		if (c.rawAccess) {
			return new ArrayList();
		}

		//c.printConstraints();
		//System.err.println("Resolving equality constraints");
		c.resolveEqualityConstraints();
		//c.printConstraints();

		//System.err.println("Resolving supertype constraints");
		c.resolveSupertypeConstraints();
		//c.printConstraints();

		//System.err.println("Resolving unresolved type arguments");
		//c.resolveBounds();
		//c.printConstraints();

		if (c.unresolvedTypeArguments()) {
			TypeDecl S = assignConvertedType();
			if (S.isUnboxedPrimitive()) {
				S = S.boxed();
			}
			TypeDecl R = decl.type();
			// TODO: replace all uses of type variables in R with their inferred types
			TypeDecl Rprime = R;
			if (R.isVoid()) {
				R = typeObject();
			}
			c.convertibleFrom(S, R);
			// TODO: additional constraints

			c.resolveEqualityConstraints();
			c.resolveSupertypeConstraints();
			//c.resolveBounds();

			c.resolveSubtypeConstraints();
		}

		return c.typeArguments();
	}

}
