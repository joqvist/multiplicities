package AST;

public enum Multiplicity {
	ANY,
	OPTION,
	BARE,
	NONE,
	ONE,
	VALUE;

	public static final int size = Multiplicity.values().length;

	@Override
	public String toString() {
		return name().toLowerCase();
	}
}

