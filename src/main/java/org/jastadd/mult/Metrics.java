package org.jastadd.mult;

import AST.*;
import java.util.Collection;
import org.jastadd.jastaddj.JastAddJVersion;

/**
 * Perform static semantic checks on a Java program.
 */
public class Metrics extends Frontend {

	/**
	 * Entry point for the Java checker.
	 * @param args command-line arguments
	 */
	public static void main(String args[]) {
		int exitCode = new Metrics().run(args);
		if (exitCode != 0) {
			System.exit(exitCode);
		}
	}

	private final JavaParser parser;
	private final BytecodeParser bytecodeParser;

	/**
	 * Initialize the Java checker.
	 */
	public Metrics() {
		super("Multiplicity Metrics", JastAddJVersion.getVersion());
		parser = new JavaParser() {
			@Override
			public CompilationUnit parse(java.io.InputStream is, String
					fileName) throws java.io.IOException,
					beaver.Parser.Exception {
				return new parser.JavaParser().parse(is, fileName);
			}
		};
		bytecodeParser = new BytecodeParser();
	}

	/**
	 * @param args command-line arguments
	 * @return {@code true} on success, {@code false} on error
	 * @deprecated Use run instead!
	 */
	@Deprecated
	public static boolean compile(String args[]) {
		return 0 == new Metrics().run(args);
	}

	/**
	 * Run the Java checker.
	 * @param args command-line arguments
     * @return 0 on success, 1 on error, 2 on configuration error, 3 on system
	 */
	public int run(String args[]) {
		int exitCode = run(args, bytecodeParser, parser);
		program.reportMetrics(System.out);
		return exitCode;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected void processErrors(Collection errors, CompilationUnit unit) {
		super.processErrors(errors, unit);
		unit.gatherMultiplicityMetrics();
	}
	@Override
	protected void processNoErrors(CompilationUnit unit) {
		unit.gatherMultiplicityMetrics();
	}
}

