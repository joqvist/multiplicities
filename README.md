Multiplicities for Java
=======================

This is an implementation of multiplicities using type annotations in Java. The
compiler is an extension to the JastAdd extensible Java compiler.

Building the Compiler
---------------------

The compiler is built using Gradle.

    gradle jar

This produces a Jar file "jastadd.jar" in the project root directory.

If you did not already have Gradle installed you can use either the "gradlew.bat" (Windows) or
"gradlew" (Unix) scripts to run it:

    ./gradlew jar
    gradlew.bat jar

Running the Compiler
====================

To compile a single file:

    java -jar jastaddj.jar Test.java

To compile multiple files, listed in a file named "files":

    java -jar jastaddj.jar @files

