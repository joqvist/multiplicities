#!/bin/bash

rsync -r --delete --filter=':- .gitignore' \
	--exclude='.git' \
	--exclude='.gitignogre' \
	--exclude='.gitmodules' \
	--exclude='.gitattributes' \
	--exclude='.svn' \
	. ~/svn/mult/compiler

pushd ../jjtest && rsync -r --delete --filter=':- .gitignore' \
	--exclude='.git' \
	--exclude='.gitignogre' \
	--exclude='.gitmodules' \
	--exclude='.gitattributes' \
	--exclude='.svn' \
	. ~/svn/mult/testmult

popd
cp jastaddj.jar ~/svn/mult/testmult
